(ns clj-common.map-test
  (:require [clojure.test :refer :all]
            [clj-common.map :refer :all]))

(deftest test-path-vals
  (let [examples [[{:a 1
                    :b 2
                    :c {:first  1
                        :second {:nested true}}}
                   {[:a]                 1
                    [:b]                 2
                    [:c :first]          1
                    [:c :second :nested] true}]]]
    (doseq [[in expected] examples]
      (is (= (path-vals in) expected))))

  (testing "Throws on invalid input"
    (is (thrown? AssertionError (path-vals [])))))

(deftest test-paths
  (let [examples [[{:a 1
                    :b 2
                    :c {:first  1
                        :second {:nested true}}}
                   '([:a]
                      [:b]
                      [:c :first]
                      [:c :second :nested])]]]
    (doseq [[in expected] examples]
      (is (= (paths in) expected))))
  )
